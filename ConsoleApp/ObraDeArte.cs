﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace ConsoleApp
{
    class Obra
    {
        public static List<Obra> obras = new List<Obra>(); //se declara lista de obras
        public int ArtId { get; set; }
        public int ArtPrice { get; set; }
        public String ArtistOwner { get; set; }
        public String ArtColors { get; set; }
        //Arrays con los atributos para obras aleatorias
        static string[] artist = {"Miguel angel", "Da vinci", "Gentileschi", "Rembrandt", "Turner", "Van gogh", "Cezanne", "Monet", "Cassatt", "Lempicka", "Picasso", "Kahlo", "okeeffe"};
        static string[] colors = { "Naranja", "Rojo", "Azul", "Verde", "Negro", "Blanco","Amarillo", "Gris", "Morado", "Rosado", "Cafe", "Veige" };
        static int[] precios = { 100000, 200000, 300000, 400000, 500000, 600000 , 700000, 800000, 900000, 1000000, 1500000, 2000000 };

        public Obra()//Constructor vacio
        {

        }

        public Obra(int artId, int artPrice, string artistOwner, string artColors) //Constructor con parametros
        {
            ArtId = artId;
            ArtPrice = artPrice;
            ArtistOwner = artistOwner;
            ArtColors = artColors;
            
        }

        public static Obra newObra()//Create
        {
            Obra art = new Obra();//se crea un nuevo objeto tipo obra
            int lastID = (obras.Last()).ArtId;//se obtiene el id del ultimo objet
            Obra newArt = new Obra();//se crea un object tipo obra
            int id = lastID + 1;
            int precio;
            string artista;
            string color;

            Console.WriteLine(" INGRESAR NUEVA OBRA DE ARTE AL SISTEMA ");
            Console.WriteLine("Ingrese precio :");
            precio = Int32.Parse(Console.ReadLine());
            Console.WriteLine("Ingrese artista :");
            artista = Console.ReadLine();
            Console.WriteLine("Ingrese color principal :");
            color = Console.ReadLine();

            art = new Obra(id, precio, artista, color); //se llama al constructor

            Console.WriteLine("Datos de la nueva obra ----> " + art);
            Console.WriteLine("Desea agregar la obra ? S/N");
            String respuesta ;
            respuesta = Console.ReadLine();
            if(respuesta == "s" || respuesta == "S")
            {
                obras.Add(art);//se añade la nueva obra
                Console.WriteLine("-----------------------------------------------------------------------------------------");
                Console.WriteLine("                    Nueva obra de arte agregada en el sistema                            ");
                Console.WriteLine("-----------------------------------------------------------------------------------------");
            }
            else if(respuesta == "n" || respuesta == "N")
            {
                Console.WriteLine("NO se agrego");
            }
            else
            {
                Console.WriteLine("Error seleccione opciones indicadas");
            }
            return art;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public static Obra UpdateObra()
        {
            Obra art = new Obra();
            Console.WriteLine(" MODIFICAR UNA OBRA PREEXISTENTE");
            Console.WriteLine(" Ingrese ID de la obra que se desea modifiar :  ");
            int searchID;
            searchID = Int32.Parse(Console.ReadLine());
            foreach (var obj in obras)//se recorren todas las obras
            {
                if(searchID == obj.ArtId)
                {
                    Console.WriteLine(" El ID ingresado " + searchID + "Coincide con la obra :");
                    Console.WriteLine(obj.ToString());//se muestra el id que coincide

                    int id = obj.ArtId;
                    int precio;
                    string artista;
                    string color;

                    Console.WriteLine("Ingrese precio :");
                    precio = Int32.Parse(Console.ReadLine());
                    Console.WriteLine("Ingrese artista :");
                    artista = Console.ReadLine();
                    Console.WriteLine("Ingrese color principal :");
                    color = Console.ReadLine();

                    art = new Obra(id, precio, artista, color); //se llama al constructor

                    Console.WriteLine("Datos Modificados----> " + art);
                    Console.WriteLine("Desea Modificar la obra definitivamente ? -> S/N");
                    String respuesta;
                    respuesta = Console.ReadLine();
                    if (respuesta == "s" || respuesta == "S")
                    {
                        obras.Add(art);//se añade la nueva obra
                        Console.WriteLine("-----------------------------------------------------------------------------------------");
                        Console.WriteLine("                    SE MODIFICO LA OBRA CON EL ID    = " + id);
                        Console.WriteLine("-----------------------------------------------------------------------------------------");
                    }
                    else if (respuesta == "n" || respuesta == "N")
                    {
                        Console.WriteLine("NO se Modifico");
                    }
                    else
                    {
                        Console.WriteLine("Error seleccione opciones indicadas");
                    }
                    return art;
                }

            }
            return art;
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        public static Obra deleteObra()
        {
            Obra art = new Obra();

            Console.WriteLine(" ELIMINAR UNA OBRA ");
            Console.WriteLine(" Ingrese ID de la obra que se desea eliniar :  ");
            int searchID;
            searchID = Int32.Parse(Console.ReadLine());
            foreach (var obj in obras)//se recorren todas las obras
            {
                if (searchID == obj.ArtId)
                {
                    Console.WriteLine(" El ID ingresado -> " + searchID + " Coincide con la obra :");
                    Console.WriteLine(obj.ToString());//se muestra el id que coincide

                    Console.WriteLine("Desea Eliminar la obra definitivamente ? -> S/N");
                    String respuesta;
                    respuesta = Console.ReadLine();
                    if (respuesta == "s" || respuesta == "S")
                    {
                        obras.RemoveAt(obras.IndexOf(obj));//se elimina el objeto
                        Console.WriteLine("-----------------------------------------------------------------------------------------");
                        Console.WriteLine("                    Se Elimino la obra con el ID    = " + searchID);
                        Console.WriteLine("-----------------------------------------------------------------------------------------");
                    }
                    else if (respuesta == "n" || respuesta == "N")
                    {
                        Console.WriteLine("NO se elimino");
                    }
                    else
                    {
                        Console.WriteLine("Error seleccione opciones indicadas");
                    }
                    return art;
                }

            }

            return art;
        }
        
        static Random rndm = new Random();//objero random
        public static Obra RandomObra(int i)//constructor de obras de arte random
        {
            Obra art = new Obra();//se crea un nuevo objeto tipo obra
            int randomArtist = rndm.Next(0, artist.Length);//se asigna aleatoriamente el atributo
            int randomColor = rndm.Next(0, colors.Length);
            int randomPrice = rndm.Next(0, precios.Length);

            art = new Obra(i, precios[randomPrice], artist[randomArtist], colors[randomColor]);
            obras.Add(art);//se añade el objeto a la coleccion
            return art;//se retorna el objeto


        }

        public Obra IsNullOrEmpty(Obra art)
        {
            return art;
        }

        //Search - linq
        public void Menor_A_MAyor()
        {
            Console.WriteLine("                             Precios Ascendientes                                       |");
            Console.WriteLine("-----------------------------------------------------------------------------------------");
            var acendiente = from art in obras
                             orderby art.ArtPrice ascending
                             select art;

            foreach (var obj in acendiente)
            {
                Console.WriteLine(obj.ToString());
            }
        }

        public void Mayor_A_Menor()//metodo ordenado por precio de las obras mayor a menor
        {
            Console.WriteLine("                             Precios Decendientes                                       |");
            Console.WriteLine("-----------------------------------------------------------------------------------------");
            var decendiente = from art in obras //se recorre la lista de obras
                          orderby art.ArtPrice descending //segun el precio decendiente
                          select art;

            foreach (var obj in decendiente)
            {
                Console.WriteLine(obj.ToString()); //se imprimen los atributos de las obras
            }

        }

        public override string ToString()
        {
            string toString = "ID: " + ArtId + "  ARTISTA: " + ArtistOwner + "  COLOR PRINCIPAL: " + ArtColors + "  PRECIO: " + ArtPrice ;
            return toString;
        }

        public void all()
        {
            Console.WriteLine("                         Todas las Obras disponibles                                    |");
            Console.WriteLine("-----------------------------------------------------------------------------------------");
            foreach (var obj in obras)//se recorren todas las obras
            {
                Console.WriteLine(obj.ToString());//se llama al metodo toString
            }
        }
    }
}
