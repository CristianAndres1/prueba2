﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace ConsoleApp
{
    class Menu
    {
        static void Main(string[] args)
        {
            //variable booleana que evalua si se ejecuta el switch
            bool menu = true;
            //variable int que guarda la opcion que se selecciona en el menu
            int opcion = 0;

            List<Obra> obras = new List<Obra>(); //lista de obras

          for (int i = 0; i < 30; i++)//se crean 30 obras random
            {
                var art = Obra.RandomObra(i+1);
                obras.Add(art);
                
            }
        
            while (menu ==true) //mientras el valor no sea falso no se cierra el menu
            {
                Console.WriteLine("------------------------------------");
                Console.WriteLine("|  SUGART     COMPANY APLICATION   |");
                Console.WriteLine("---------------MENU-----------------");
                Console.WriteLine("|----  SELLECIONE UNA OPCION : ----|");
                Console.WriteLine("1)  Ingresar Obra de Arte          |");
                Console.WriteLine("2)  Modificar Obra de Arte         |");
                Console.WriteLine("3)  Eliminar Obra de Arte          |");
                Console.WriteLine("4)  Buscar Obra de Arte            |");
                Console.WriteLine("5)  Ingresar a una Subasta         |");
                Console.WriteLine("6)  SALIR DE LA APP                |");
                Console.WriteLine("|----------------------------------|");
                Console.WriteLine("Ingrese un numero                  |");
                
                Obra view = new Obra();//se crea un object tipo obra
                try
                {
                    opcion = Int32.Parse(Console.ReadLine()); //se convierte el input string en int
                    
                    switch (opcion)
                    {
                       
                        case 1:
                            //Create 
                            Console.Clear();
                            try
                            {
                                Console.WriteLine(" ");
                                Console.WriteLine(" ");
                                Console.WriteLine("AGREGAR NUEVA OBRA DE ARTE AL SISTEMA :");
                                var newArt = Obra.newObra();
                                obras.Add(newArt);
                            }
                            catch (Exception )
                            {
                                Console.WriteLine("ERROR DE INGRESO");
                            }

                            view.all();// se listan todas las obras 
                            break;

                        case 2:
                            //Update
                            Console.Clear();
                            view.all();
                            Console.WriteLine("-----------------------------------------------------------------------------------------");
                            Console.WriteLine("-----------------------------------------------------------------------------------------");
                            var updateArt = Obra.UpdateObra();
                            obras.Add(updateArt);
                            break;

                        case 3:
                            //Delete
                            Console.Clear();
                            view.all();
                            Obra.deleteObra();
                            break;

                        case 4:
                            //Search - linq
                            Console.Clear();
                            Console.WriteLine("Ver todas las obras de atre guardadas -> 1");
                            Console.WriteLine("Ver desde la mas cara a la mas barata -> 2");
                            Console.WriteLine("Ver desde la mas barata a la mas cara -> 3");
                            Console.WriteLine(" ");
                            Console.WriteLine("Ingrese orden :");
                            int searchView;
                            searchView = Int32.Parse(Console.ReadLine()); //se gurda la opcion seleccionada
                            try
                            {
                                
                                switch (searchView)
                                {

                                    case 1:
                                        Console.WriteLine("-----------------------------------------------------------------------------------------");
                                        view.all();// se llaman  los metodos necesarios
                                        Console.WriteLine("-----------------------------------------------------------------------------------------");
                                        Console.WriteLine(" ");
                                        Console.WriteLine(" ");
                                        Console.WriteLine(" ");
                                        break;
                                    case 2:
                                        Console.WriteLine("-----------------------------------------------------------------------------------------");
                                        view.Mayor_A_Menor();
                                        Console.WriteLine("-----------------------------------------------------------------------------------------");
                                        Console.WriteLine(" ");
                                        Console.WriteLine(" ");
                                        Console.WriteLine(" ");
                                        break;
                                    case 3:
                                        Console.WriteLine("-----------------------------------------------------------------------------------------");
                                        view.Menor_A_MAyor();
                                        Console.WriteLine("-----------------------------------------------------------------------------------------");
                                        Console.WriteLine(" ");
                                        Console.WriteLine(" ");
                                        Console.WriteLine(" ");
                                        break;
                  
                                    default:
                                        Console.WriteLine("------------------------------------------");
                                        Console.WriteLine("Ingresar numero de opciones entre  1 y 3 ");
                                        Console.WriteLine("------------------------------------------");
                                        Console.WriteLine("Vista por defecto (todas las obras disponibles) : ");
                                        Console.WriteLine(" ");
                                        Console.WriteLine("------------------------------------------");
                                        view.all();
                                        return;
                                }

                            }
                            catch (Exception)
                            {
                                Console.WriteLine("ERROR DE INGRESO");
                            }

                            break;

                        case 5:
                            //Subasta
                            Console.Clear();
                            Console.WriteLine("Coming soon");
                            break;

                        case 6:
                            menu = false;
                            Console.WriteLine("** EXIT **");
                            Environment.Exit(0);
                            break;

                        default:
                            Console.WriteLine("------------------------------------------");
                            Console.WriteLine("Ingresar numero de opciones entre  1 y 5 ");
                            Console.WriteLine("------------------------------------------");
                            break;
                    }
                }
                catch (Exception)//Error general
                {
                    Console.Clear(); //se limpia el texto anterior de la consola
                    Console.WriteLine("ERROR DE INGRESO"); 
                }

            }

        }
    }
}
